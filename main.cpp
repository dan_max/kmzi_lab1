#include "uniston.h"
#include <fstream>

using namespace std;
int main(int argc, char* argv[]) {
	string key0 = "-p_in";
	string key1 = "-p_out";
	string key2 = "-in";
	string key3 = "-s";
	string key4 = "-d";
	string key5 = "-e";
	string key6 = "-h";
	string key7 = "-g";
	string file_in_path, file_out_path,in,secret_word1,secret_word2;
	bool keys[70] = { false };
	for (int i = 0; i < argc; i++) {
		if (argv[i] == key0 ) {
			file_in_path = argv[i + 1];
			keys[0] = true;
		}
		if (argv[i] == key1) {
			file_out_path = argv[i + 1];
			keys[1] = true;
		}
		if (argv[i] == key2) {
			in = argv[i + 1];
			keys[2] = true;
		}
		if (argv[i] == key3) {
			keys[3] = true;
		}
		if (argv[i] == key4) {
			keys[4] = true;
		}
		if (argv[i] == key5) {
			keys[5] = true;
		}
		if (argv[i] == key6) {
			cout << "Help:\n\t-p_in Path to input file.\n\t-p_out Path to output file.\n\t-in Input string.\n\t-s Show the matrix`s.\n\t-d Decript.\n\t-e Encript.\n\t-h Help.\nWarning!!!!\nNot use -p_in and -in together.\nNot use -d and -e together.\nThx.\n";
			return 1;
		}
		if (argv[i] == key7) {
			keys[7] = true;
			secret_word1 = argv[i + 1];
			secret_word2 = argv[i + 2];
		}
	}
	if(keys[2] xor keys[0]){
		Uniston cp(keys[3]);
		if (keys[7]) {
			cp.generate(secret_word1, secret_word2);
		}
		else {
			cp.generate();
		}
		string res;
		if (keys[2]) {
			if (keys[5]) {
				res = cp.encrypt(in);
			}
			if (keys[4]) {
				res = cp.decrypt(in);
			}
		}
		if (keys[0]) {
			ifstream fin(file_in_path, ios_base::in);
			if (!fin.is_open()) {
				cout << "file open error" << endl;
				return -1;
			}
			string temp;
			while (!fin.eof()) {
				getline(fin, temp);
				in += temp;
			}
			fin.close();
			if (keys[5]) {
				res = cp.encrypt(in);
			}
			if (keys[4]) {
				res = cp.decrypt(in);
			}
		}
		cout << res << endl;
		if (keys[1]) {
			ofstream fo(file_out_path, ios_base::trunc);
			if (!fo.is_open()) {
				cout << "file open error" << endl;
				return -1;
			}
			fo << res << endl;
			fo.close();
		}
		return 0;
	}
	else {
		string res = "secret word";
		Uniston cp(keys[3]);
		cout << res << endl;
		string out = cp.encrypt(res);
		cout << out << endl;
		string out1 = cp.decrypt(out);
		cout << out1 << endl;
		return 0;
	}
}