#ifndef uinston_h
#define uinston_h

#define size_m 8

#include <iostream>
#include <string>
#include <set>

using namespace std;

class Uniston {
private:
	string alp = { 'a','b','c','d','e','f','g','h','i','j','k','l',
		'm','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E',
		'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X',
		'Y','Z','0','1','2','3','4','5','6','7','8','9',' ','.' };

	int	**matrix1, **matrix2;
	bool show;
	void mix() {
		char temp;
		int t;
		for (int i = 0; i < size_m*size_m; i++) {
			temp = alp[i];
			t = rand() % (size_m*size_m);
			alp[i] = alp[t];
			alp[t] = temp;
		}
	}
	int find(char in, int **matrix) {//+++
		for (int i = 0; i < size_m; i++) {
			for (int j = 0; j < size_m; j++) {
				if (matrix[i][j] == in) {
					return i * size_m + j;
				}
			}
		}
		return -1;
	}
	int mod(int in, int m) {
		return (in + m) % m;
	}
public:
	Uniston(bool show_) {//+++
		show = show_;
	}	
	~Uniston() {
		for (int i = 0; i < size_m; i++) {
			delete[] matrix1[i];
			delete[] matrix2[i];
		}
		delete[] matrix1;
		delete[] matrix2;
	}
	string delete_rep(string v) {
		set<char> chars;
		string res = v;
		int i = 0;
		while (i < res.length()) {
			if (chars.count(res[i]) == 0) {
				chars.insert(res[i]);
				i++;
			}
			else if (chars.count(res[i]) > 0) {
				res.erase(i, 1);
			}
			else {
				i++;
			}
		}
		return res;
	}
	void generate(string s1, string s2) {
		matrix1 = new int *[size_m];
		matrix2 = new int *[size_m];
		for (int i = 0; i < size_m; i++) {
			matrix1[i] = new int[size_m];
			matrix2[i] = new int[size_m];
		}
		s1 = delete_rep(s1);
		s2 = delete_rep(s2);
		for (int i = 0; i < s1.length(); i++) {
			matrix1[i / size_m][i % size_m] = s1[i];
		}
		string alp_copy = alp;
		for (int i = 0; i < s1.length(); i++) {
			if (alp_copy.find(s1[i]) != string::npos) {
				alp_copy.erase(alp_copy.find(s1[i]), 1);
			}
		}
		int count = 0;
		bool is_first_time = true;
		for (int i = 0; i < size_m; i++) {
			for (int j = 0; j < size_m; j++) {
				if (is_first_time) {
					i = s1.length() / size_m;
					j = s1.length() % size_m;
					is_first_time = false;
				}
				matrix1[i][j] = alp_copy[count];
				count++;
			}
		}
		for (int i = 0; i < s2.length(); i++) {
			matrix2[i / size_m][i % size_m] = s2[i];
		}
		alp_copy = alp;
		for (int i = 0; i < s2.length(); i++) {
			if (alp_copy.find(s2[i]) != string::npos) {
				alp_copy.erase(alp_copy.find(s2[i]), 1);
			}
		}
		is_first_time = true;
		count = 0;
		for (int i = 0; i < size_m; i++) {
			for (int j = 0; j < size_m; j++) {
				if (is_first_time) {
					i = s2.length() / size_m;
					j = s2.length() % size_m;
					is_first_time = false;
				}
				matrix2[i][j] = alp_copy[count];
				count++;
			}
		}
		if (show) {
			string res;
			for (int i = 0; i < size_m; i++) {
				for (int j = 0; j < size_m; j++) {
					res += matrix1[i][j];
					res += ' ';
				}
				res += '\t';
				for (int k = 0; k < size_m; k++) {
					res += matrix2[i][k];
					res += ' ';
				}
				res += '\n';
			}
			res += '\n';
			cout << res;
		}
	}
	void generate() {
		matrix1 = new int *[size_m];
		matrix2 = new int *[size_m];
		for (int i = 0; i < size_m; i++) {
			matrix1[i] = new int[size_m];
			matrix2[i] = new int[size_m];
		}
		mix();
		for (int i = 0; i < size_m; i++) {
			for (int j = 0; j < size_m; j++) {
				matrix1[i][j] = alp[i * size_m + j];
			}
		}
		mix();
		for (int i = 0; i < size_m; i++) {
			for (int j = 0; j < size_m; j++) {
				matrix2[i][j] = alp[i * size_m + j];
			}
		}
		if (show) {
			string res;
			for (int i = 0; i < size_m; i++) {
				for (int j = 0; j < size_m; j++) {
					res += matrix1[i][j];
					res += ' ';
				}
				res += '\t';
				for (int k = 0; k < size_m; k++) {
					res += matrix2[i][k];
					res += ' ';
				}
				res += '\n';
			}
			res += '\n';
			cout << res;
		}
	}
	string encrypt(string in) {
		if (in.length() % 2 == 1) {
			in.append(" ");
		}
		string res = in;
		for (int i = 1; i < in.length(); i += 2) {
			int out = find(in[i - 1], matrix1);
			int y1 = out / size_m;
			int x1 = out % size_m;

			out = find(in[i], matrix2);
			int y2 = out / size_m;
			int x2 = out % size_m;
			if (y1 == y2) {
				res[i - 1] = matrix2[y2][mod(x2 + 1, size_m)];
				res[i] = matrix1[y2][mod(x1 - 1, size_m)];
			}
			else {
				res[i - 1] = matrix2[y1][x2];
				res[i] = matrix1[y2][x1];
			}
		}
		return res;
	}
	string decrypt(string in) {//+++
		string res = in;
		for(int i = 1; i < in.length(); i += 2) {
			int index = find(in[i - 1], matrix2);
			int x1, x2, y1, y2;
			y1 = index / size_m;
			x1 = index % size_m;
			index = find(in[i], matrix1);
			y2 = index / size_m;
			x2 = index % size_m;
			if (y1 == y2) {
				res[i - 1] = matrix1[y2][mod(x2 + 1, size_m)];
				res[i] = matrix2[y2][mod(x1 - 1, size_m)];
			}
			else {
				res[i - 1] = matrix1[y1][x2];
				res[i] = matrix2[y2][x1];
			}
		}
		return res;
	}
};
#endif uinston_h
